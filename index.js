require('custom-env').env('local');
const TELEGRAM_API = require('node-telegram-bot-api');
const axios = require('axios');

const TOKEN = process.env.TELEGRAM_TOKEN;
const BOT = new TELEGRAM_API(TOKEN, {polling: true});

const getCatsImages = async (chatID) => {
  const cats = [];
  let url = "";
  await axios.get('https://api.thecatapi.com/v1/images/search').then((response) => {
    url = response.data[0].url;
    cats.push(url);
  }).catch((e) => console.error(e));

  let lastItem = cats[cats.length - 1];
  console.log('lastItem: ', lastItem);
  await BOT.sendPhoto(chatID, lastItem);

  await setTimeout(() => { 
    getCatsImages(chatID);
  }, 3600000);
  
};

const start = async () => {
  await BOT.on('message', async (msg) => {
    if(msg.text === '/start') { 
      setTimeout(() => { 
        getCatsImages(msg.chat.id);
      }, 2000);
    }
  });
};

start();
